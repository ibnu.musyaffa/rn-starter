import {AppRegistry} from 'react-native';
import React, {Component} from 'react';
import 'react-native-gesture-handler';
import {name as appName} from './app.json';
import App from './src/App';
import NavigationService from './src/helper/NavigationService';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {
  ApplicationProvider as KittenProvider,
  Layout,
  Text,
  IconRegistry,
} from '@ui-kitten/components';
import {mapping, light as lightTheme} from '@eva-design/eva';

function Main() {
  return (
    <KittenProvider mapping={mapping} theme={lightTheme}>
      <IconRegistry icons={EvaIconsPack} />
      <App
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    </KittenProvider>
  );
}

AppRegistry.registerComponent(appName, () => Main);
