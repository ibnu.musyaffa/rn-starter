import axios from 'axios';
import url from '../config/url';
import header from '../config/header';
class todo {
  static index() {
    const headers = header();
    return axios.get(`${url}/todo`, {headers});
  }
}

export default todo;
