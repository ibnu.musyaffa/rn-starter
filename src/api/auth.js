import axios from 'axios';
import url from '../config/url';

class auth {
  static login({username, password}) {
    const headers = {
      Accept: 'application/json',
    };

    let bodyFormData = new FormData();
    bodyFormData.append('username', username);
    bodyFormData.append('password', password);
    return axios.post(`${url}/auth/token`, bodyFormData, headers);
  }
}

export default auth;
