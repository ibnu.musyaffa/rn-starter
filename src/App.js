import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import AuthFilter from './AuthFilter'
import AuthStack from './AuthStack'
import RootStack from './RootStack'
const App = createAppContainer(
  createSwitchNavigator({
    AuthFilter,
    AuthStack,
    RootStack
  })
)

export default App
