import React, { Component } from 'react'
import styled from 'styled-components/native'
import Icon from 'react-native-vector-icons/MaterialIcons'

const ErrorMessage = styled.View`
  background-color: ${(props) => (props.bgColor ? props.bgColor : '#ffd8d8')};
  height: auto;
  margin-bottom: 10px;
  border-radius: 5px;
  border-color: ${(props) => (props.border ? props.border : '#0cd11b')};
  border-width: 1px;
  align-items: center;
  padding: 10px;
  flex-direction: row;
`

const TextErrorMessage = styled.Text`
  margin-left: 5px;
  margin-right: 5px;
  height: auto;
  padding: 1px;
  width: 300px;
`

export default class Alert extends Component {
  render() {
    const type = this.props.type

    if (type == 'error') {
      return (
        <ErrorMessage bgColor='#ffd8d8' border='red'>
          <Icon name='error' size={25} color='#ff2626' />
          <TextErrorMessage>{this.props.message}</TextErrorMessage>
        </ErrorMessage>
      )
    }

    if (type == 'success') {
      return (
        <ErrorMessage bgColor='#e0ffe2'>
          <Icon name='check' size={25} color='#42ff4e' border='#0cd11b' />
          <TextErrorMessage>{this.props.message}</TextErrorMessage>
        </ErrorMessage>
      )
    }

    return (
      <ErrorMessage bgColor='#ffd8d8' border='red'>
        <Icon name='error' size={25} color='#ff2626' />
        <TextErrorMessage>Lorem ipsum</TextErrorMessage>
      </ErrorMessage>
    )
  }
}
