import React, {Component} from 'react';
import {View, Alert} from 'react-native';
import {Button} from '@ui-kitten/components';
import AsyncStorage from '@react-native-community/async-storage';
import NavigationService from '../../helper/NavigationService';
import todo from '../../api/todo';
export class Home extends Component {
  constructor(props) {
    super(props);

    this.onLogout = this.onLogout.bind(this);
  }

  componentDidMount() {
    this.todo();
  }

  async todo() {
    try {
      let res = await todo.index();
      console.log(res);
    } catch (error) {
      console.log(error);
    }
  }

  onLogout() {
    Alert.alert(
      'Konfirmasi',
      'Apakah anda yakin untuk logout ?',
      [
        {
          text: 'Cancel',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            AsyncStorage.clear();
            NavigationService.navigate('AuthStack');
          },
        },
      ],
      {cancelable: false},
    );
  }
  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Button mode="contained" onPress={this.onLogout}>
          Logout
        </Button>
      </View>
    );
  }
}

export default Home;
