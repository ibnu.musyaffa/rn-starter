import * as React from 'react';
import styled from 'styled-components/native';
import AsyncStorage from '@react-native-community/async-storage';

import Alert from '../../components/Alert';
import auth from '../../api/auth';
import LoginForm from './LoginForm';

const Container = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1;
`;

const Content = styled.View`
  flex-direction: column;
  width: 100%;
  padding-left: 25px;
  padding-right: 25px;
`;

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);

    this.setToken = this.setToken.bind(this);

    this.state = {
      isError: false,
      errorMessage: '',
    };
  }

  setToken = async token => {
    try {
      await AsyncStorage.setItem('userToken', token);
    } catch (error) {
      //
    }
  };

  async submit(values, actions) {
    this.setState({
      isError: false,
      errorMessage: '',
    });

    try {
      let res = await auth.login(values);
      console.log(res);
      actions.setSubmitting(false);
      let token = res.data.token;
      this.setToken(token);
      global.userToken = token;
      this.props.navigation.navigate('RootStack');
    } catch (error) {
      console.log(error.response);
      this.setState({
        isError: true,
        errorMessage: 'Koneksi error',
      });
      actions.setSubmitting(false);
    }
  }

  render() {
    return (
      <Container>
        <Content>
          {this.state.isError && (
            <Alert type="error" message={this.state.errorMessage} />
          )}
          <LoginForm onSubmit={this.submit} onForgot={this.toForget} />
        </Content>
      </Container>
    );
  }
}
export default Login;
