import React, {Component} from 'react';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {Button} from '@ui-kitten/components';
import {Input} from '@ui-kitten/components';

const loginSchema = Yup.object().shape({
  username: Yup.string().required('Username Wajib di isi'),
  password: Yup.string().required('Password wajib diisi'),
});

class LoginForm extends Component {
  render() {
    return (
      <Formik
        initialValues={{username: '', password: ''}}
        onSubmit={this.props.onSubmit}
        validationSchema={loginSchema}>
        {({
          handleChange,
          handleSubmit,
          handleBlur,
          values,
          errors,
          touched,
          isSubmitting,
        }) => (
          <React.Fragment>
            <Input
              onChangeText={handleChange('username')}
              onBlur={handleBlur('username')}
              status={errors.username && touched.username ? 'danger' : 'basic'}
              value={values.username}
              placeholder="username atau No.Handphone"
              caption={
                errors.username && touched.username ? errors.username : ''
              }
              error={errors.username && touched.username}
              style={{marginBottom: 15}}
            />

            <Input
              onChangeText={handleChange('password')}
              onBlur={handleBlur('password')}
              status={errors.password && touched.password ? 'danger' : 'basic'}
              value={values.password}
              placeholder="Masukkan Password Anda"
              secureTextEntry={true}
              style={{marginBottom: 15}}
              caption={
                errors.password && touched.password ? errors.password : ''
              }
              error={errors.password && touched.password}
            />

            <Button
              dark={true}
              mode="contained"
              onPress={handleSubmit}
              disabled={isSubmitting}>
              {isSubmitting ? 'Mohon tunggu...' : 'Submit'}
            </Button>
          </React.Fragment>
        )}
      </Formik>
    );
  }
}

export default LoginForm;
