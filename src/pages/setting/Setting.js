import React, { Component } from 'react'
import { View, StyleSheet, ScrollView } from 'react-native'

import {
  CheckBox,
  Toggle,
  Card,
  Button,
  Input,
  Select,
  Datepicker,
  Spinner,
  Icon,
  List,
  ListItem,
} from '@ui-kitten/components'
import { Text } from '@ui-kitten/components'
import styled from 'styled-components/native'

const Container = styled.View`
  justify-content: center;
  align-items: center;
  margin-bottom: 20px;
`

const data = new Array(3).fill({
  title: 'Title for Item',
  description: 'Description for Item',
})

export const ListCompositeItemShowcase = () => {
  const renderItemAccessory = (style) => <Button style={style}>FOLLOW</Button>

  const renderItemIcon = (style) => <Icon {...style} name='person' />

  const renderItem = ({ item, index }) => (
    <ListItem
      title={`${item.title} ${index + 1}`}
      description={`${item.description} ${index + 1}`}
      icon={renderItemIcon}
      accessory={renderItemAccessory}
    />
  )

  return <List data={data} renderItem={renderItem} />
}

const Selectdata = [{ text: 'Option 1' }, { text: 'Option 2' }, { text: 'Option 3' }]
export class Setting extends Component {
  render() {
    return (
      <ScrollView style={{ padding: 30 }}>
        <Container>
          <CheckBox />
        </Container>
        <Container>
          <Toggle />
        </Container>
        <Container>
          <Text>Sample Text</Text>
        </Container>
        <Container>
          <Text style={styles.text} category='h1'>
            H1
          </Text>

          <Text style={styles.text} category='h2'>
            H2
          </Text>

          <Text style={styles.text} category='h3'>
            H3
          </Text>

          <Text style={styles.text} category='h4'>
            H4
          </Text>

          <Text style={styles.text} category='h5'>
            H5
          </Text>

          <Text style={styles.text} category='h6'>
            H6
          </Text>
        </Container>
        <Container>
          <Card>
            <Text>
              The Maldives, officially the Republic of Maldives, is a small
              country in South Asia, located in the Arabian Sea of the Indian
              Ocean. It lies southwest of Sri Lanka and India, about 1,000
              kilometres (620 mi) from the Asian continent
            </Text>
          </Card>
        </Container>
        <View
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            padding: 8,
          }}
        >
          <Button style={styles.text} status='primary'>
            PRIMARY
          </Button>

          <Button style={styles.text} status='success'>
            SUCCESS
          </Button>

          <Button style={styles.text} status='info'>
            INFO
          </Button>

          <Button style={styles.text} status='warning'>
            WARNING
          </Button>

          <Button style={styles.text} status='danger'>
            DANGER
          </Button>

          <Button style={styles.text} status='basic'>
            BASIC
          </Button>
        </View>
        <Container>
          <Input size='small' placeholder='Place your Text' />
        </Container>
        <View style={{ marginBottom: 20 }}>
          <Select data={Selectdata} />
        </View>
        <View style={{ marginBottom: 20 }}>
          <Datepicker />
        </View>
        <Container>
          <Spinner status='primary' />
        </Container>
        <ListCompositeItemShowcase />
        <View style={{ height: 100 }} />
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    margin: 8,
  },
})

export default Setting
