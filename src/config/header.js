const getHeader = () => {
  return {
    Authorization: global.userToken,
    Accept: 'application/json',
  };
};

export const headerFormEncode = () => {
  return {
    Authorization: global.userToken,
    'Content-Type': 'application/x-www-form-urlencoded',
    Accept: 'application/json',
  };
};

export default getHeader;
