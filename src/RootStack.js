import React from 'react'
import { SafeAreaView } from 'react-navigation'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import {
  BottomNavigation,
  BottomNavigationTab,
  Icon,
} from '@ui-kitten/components'
import Home from './pages/home/Home'
import Setting from './pages/setting/Setting'

const PersonIcon = (style) => <Icon {...style} name='person-outline' />

const BellIcon = (style) => <Icon {...style} name='bell-outline' />

const TabBarComponent = ({ navigation }) => {
  const onSelect = (index) => {
    const selectedTabRoute = navigation.state.routes[index]
    navigation.navigate(selectedTabRoute.routeName)
  }

  return (
    <SafeAreaView>
      <BottomNavigation
        // style={styles.bottomNavigation}
        selectedIndex={navigation.state.index}
        onSelect={onSelect}
      >
        <BottomNavigationTab icon={PersonIcon} />
        <BottomNavigationTab icon={BellIcon} />
      </BottomNavigation>
    </SafeAreaView>
  )
}

const TabNavigator = createBottomTabNavigator(
  {
    Home,
    Setting,
  },
  {
    tabBarComponent: TabBarComponent,
  },
)
export default TabNavigator
