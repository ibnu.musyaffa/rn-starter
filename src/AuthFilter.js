import React, { Component } from 'react'
import { ActivityIndicator, View } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
class AuthFilter extends Component {
  constructor(props) {
    super(props)
    this.checkAuth()
  }

  checkAuth = async () => {
    const userToken = await AsyncStorage.getItem('userToken')

    if (userToken != null) {
      global.userToken = userToken
    }

    // setTimeout(() => {
    this.props.navigation.navigate(userToken ? 'RootStack' : 'AuthStack')
    // }, 600);
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <ActivityIndicator style={{ marginTop: 60 }} size={30} />
      </View>
    )
  }
}

export default AuthFilter
