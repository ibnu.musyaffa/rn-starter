module.exports = {
  "parser": "babel-eslint",
  "plugins": [
    "react",
    "import",
  ],
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended"
  ],
  'rules': {
    "array-bracket-spacing": [
      "error",
      "never"
    ],
    "lines-between-class-members": 0,
    "jsx-quotes": ["error", "prefer-single"],
    "object-curly-spacing": [
      "error",
      "always"
    ],
    "block-spacing": "error",
    "comma-spacing": [
      "error",
      {
        "before": false,
        "after": true
      }
    ],
    "func-call-spacing": [
      "error",
      "never"
    ],
    "key-spacing": [
      "error",
      {
        "beforeColon": false,
        "afterColon": true
      }
    ],
    "max-len": [
      "error",
      {
        "code": 500,
        "ignoreUrls": true,
        "ignoreStrings": true,
        "ignoreTemplateLiterals": true,
        "ignoreRegExpLiterals": true
      }
    ],
    "no-mixed-spaces-and-tabs": "error",
    "no-multiple-empty-lines": [
      "error",
      {
        "max": 1
      }
    ],
    "no-trailing-spaces": "error",
    "no-unneeded-ternary": "error",
    "one-var-declaration-per-line": [
      "error",
      "always"
    ],
    "space-before-blocks": [
      "error",
      "always"
    ],
    "indent": [
      "error",
      2
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "single"
    ],
    "semi": [
      "error",
      "never"
    ],
    "no-const-assign": "error",
    "no-console": "error",
    "no-dupe-class-members": "error",
    "no-this-before-super": "error",
    "no-unused-vars": ["error", { "args": "none" }],
    "no-multi-spaces": "error",
    "no-var": "error",
    "no-await-in-loop": "error",
    "no-constant-condition": "error",
    "prefer-spread": "warn",
    "prefer-template": "warn",
    "require-yield": "error",
    "arrow-parens": "warn",
    "arrow-spacing": [
      "error",
      {
        "before": true,
        "after": true
      }
    ],
    "no-undef": 0,
    "generator-star-spacing": "warn",
    "object-shorthand": "warn",
    "react/jsx-closing-bracket-location": "warn",
    "react/jsx-curly-spacing": "warn",
    "react/jsx-no-duplicate-props": "warn",
    "react/jsx-no-undef": "warn",
    "react/jsx-uses-react": "error",
    "react/jsx-uses-vars": "error",
    "react/jsx-wrap-multilines": "warn",
    "react/no-direct-mutation-state": "warn",
    "react/prop-types": "off",
    "react/react-in-jsx-scope": "error",
    "react/self-closing-comp": "warn",
    "react/sort-comp": "warn",
    "react/no-string-refs": "off",
    "react/prefer-stateless-function": 0,
    "react/jsx-first-prop-new-line": 2,
    "import/order": [
      "error"
    ],
  }
};
